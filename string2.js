function splitIpComp(inputString){
    let outputString = inputString.split('.');
    if(outputString.length != 4) return []; 
    for(let comp = 0; comp < outputString.length; comp++){
        if(numCheck(outputString[comp])){
            let tempComp = parseInt(outputString[comp]);
            if(tempComp >= 0 && tempComp <= 255){
                continue;
            } else{
                return [];
            }
        }
    }
    return outputString;
}

// checks if all the characters in a given string are numbers.
function numCheck(inputStr){
    for(let strIndex = 0; strIndex < inputStr.length; strIndex++){
        if(inputStr[strIndex] >= 0 && inputStr[strIndex] <= 9){
            continue;
        } else{
            return false;
        }
    }
    return true;
}

module.exports = {splitIpComp : splitIpComp};