function sentence(inputArr){
    if(!inputArr) return '';
    let finalStr = '';
    for(let word in inputArr){
        if(inputArr[word]){
            finalStr += inputArr[word] + ' ';
        } else {
            return '';
        }
    }
    return finalStr;
}

module.exports = {sentence : sentence};