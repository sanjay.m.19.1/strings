function monthInWords(inputString){
    if(!inputString) return '';
    var monthNames = [ "January", "February", "March", "April", "May", "June", 
    "July", "August", "September", "October", "November", "December" ];
    let month = parseInt(inputString.split('/')[0]);
    month--;
    return month >= 0 && month <= 11 ? monthNames[month] : "invalid formate";
}

module.exports = {monthInWords};