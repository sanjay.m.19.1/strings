function parseValue(inputString){
    if(!inputString) return 0;
    let tempStr = '';
    if(inputString[0] == '-'){      //cheack if the input value is signed or unsigned.
        tempStr = '-' + inputString.slice(2, );
    } else {
        tempStr = inputString.slice(1, );
    }
    if(!tempStr.match(/,,+|,\.|\.,/gm)){
        tempStr = tempStr.split(',').join('');
        if(isNaN(tempStr)){
            return 0;
        } else return parseFloat(tempStr);
    } else return 0;
}

module.exports = {parseValue};