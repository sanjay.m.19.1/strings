function fullName(nameObj){
    let outputStr = '';
    let { first_name: firstName, middle_name: middleName, last_name: lastName } = nameObj;
    //if there is no first name or seconde name return null.
    if(!firstName || !lastName) return '';
    if(middleName) {
        outputStr = `${firstName} ${middleName} ${lastName}`;
    } else {
        outputStr = `${firstName} ${lastName}`;
    }
    return toTitleCase(outputStr);
}

//function takes string and return it in title case.
function toTitleCase(inputStr){
    strArr = inputStr.split(' ')
    for(let word in strArr){
        strArr[word] = strArr[word].toLowerCase().split('');
        strArr[word][0] = strArr[word][0].toUpperCase();
        strArr[word] = strArr[word].join('');
    }
    return strArr.join(' ');
}

module.exports = {fullName : fullName};